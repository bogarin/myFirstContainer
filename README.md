## Docker Compose
para saber como funciona lo que es docker compose de una forma basica siguiento los pasos dela pagina [x-team](https://x-team.com/blog/docker-compose-php-environment-from-scratch/)


## Project setup
```
ahoy up ó docker-compose up
```
## Project Build
```
docker-compose build
```
## Project Destroy
```
ahoy destroy
```
## Install Docker 
[Docker](https://docs.docker.com/install/linux/docker-ce/ubuntu/#install-from-a-package)

## Install Docker Compose
[Docker-compose](https://docs.docker.com/compose/install/#install-compose)

## Install ahoy-cli
[ahoy](https://github.com/ahoy-cli/ahoy)